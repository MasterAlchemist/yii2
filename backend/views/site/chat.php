<?php

use yii\web\View;

?>

<div class="col-md-12">

    <div id="chat" class="col-md-12 chat-window">

    <?php foreach($messages as $message): ?>

        <?php if($message->created_by == Yii::$app->user->id){ ?>
                <div class="chat-bubble"><span class="chat-bubble-body chat-bubble-blue"><b><?= $message->createdBy->username ?></b>: <?= $message->message ?></span><p><?= date("Y-m-d H:i:s", $message->created_at) ?></p></div>
        <?php } else { ?>
                <div class="chat-bubble"><span class="chat-bubble-body"><b><?= $message->createdBy->username ?></b>: <?= $message->message ?></span><p><?= date("Y-m-d H:i:s", $message->created_at) ?></p></div>
        <?php } ?>

    <?php endforeach; ?>

    </div>

    <div class="col-md-12">
        <div class="input-group mt-3">
            <input id="message" type="text" class="form-control" placeholder="Type your message here">
            <div class="input-group-append">
                <button id="btnSend" class="btn btn-outline-secondary" type="button">Send</button>
            </div>
        </div>
    </div>

</div>

<?php $this->registerJs('
$(function() {
    const audio = new Audio("/pop.wav");
    var username = "'.Yii::$app->user->identity->username.'";
    var chat = new WebSocket("ws://localhost:8080");

    chat.onmessage = function(e) {
        var response = JSON.parse(e.data);
        if (response.type && response.type == "chat") {
            if(response.from == username){
                $("#chat").append("<div class=\"chat-bubble\"><span class=\"chat-bubble-body chat-bubble-blue\"><b>" + response.from + "</b>: " + response.message + "</span><p>"+response.created_at+"</p></div>");
            } else{
                $("#chat").append("<div class=\"chat-bubble\"><span class=\"chat-bubble-body\"><b>" + response.from + "</b>: " + response.message + "</span><p>"+response.created_at+"</p></div>");
            }
        } else if(response.message){
            $("#chat").append("<div class=\"chat-bubble\"><span class=\"chat-notice\">" + response.message + "</span></div>");
        }
        audio.play();
        $("#chat").scrollTop($("#chat")[0].scrollHeight);
    };

    chat.onopen = function(e) {
        console.log("Connection established")
        chat.send( JSON.stringify({"action" : "setName", "name" : username}));
    };

    $("#btnSend").click(sendMessage);
    $("#message").keyup(function(e){ 
        var code = e.key;
        if(code==="Enter"){ 
            e.preventDefault();
            sendMessage();
        }
    });

    function sendMessage() {
        if ($("#message").val()) {
            chat.send( JSON.stringify({"action" : "chat", "message" : $("#message").val()}) );
            $("#message").val("");
            $("#message").focus();
        }
    }

})
', View::POS_END); ?>