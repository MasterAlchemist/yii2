<?php

use yii\bootstrap4\Nav;

?>
<aside class="shadow">
    <?php echo Nav::widget([
        'options' => [
            'class' => 'd-flex flex-column nav-pills'
        ],
        'items' => [
            [
                'label' => 'Dashboard',
                'url' => ['/site/index']
            ],
            [
                'label' => 'Categories',
                'url' => ['/category/index']
            ],
            [
                'label' => 'Videos',
                'url' => ['/video/index']
            ],
            [
                'label' => 'Chat',
                'url' => ['/site/chat']
            ]
        ]
    ]) ?>
</aside>