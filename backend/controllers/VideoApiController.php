<?php

namespace backend\controllers;

use common\models\Video;
use yii\rest\ActiveController;

class VideoApiController extends ActiveController
{
    public $modelClass = Video::class;

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['create']);
        return $actions;
    }
    
    public function actionCreate()
    {
        return "Hola";
    }
}
