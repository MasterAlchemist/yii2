<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'video/update/<id>' => 'video/update',
                ['class' => 'yii\rest\UrlRule', 'controller' => ['tests' => 'video-api'], 'except' => ['view']],
                'GET,HEAD tests/<id:[\w-]+>' => 'video-api/view',
            ],
        ],
        'assetManager' => [
            'appendTimestamp' => true
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'live' => [
                    'class' => 'yii\authclient\clients\Live',
                    'clientId' => '403c4531-2b7c-455a-8da0-c68d170f2f10',
                    'clientSecret' => 'TvE7Q~NWHsVIK6ZXJuthIyYwL8.NyC7Yq5RiB',
                    'returnUrl' => 'http://localhost/FreeCodeTube/backend/web/index.php?r=site%2Fauth&authclient=live'
                ],
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => '3046184835603802',
                    'clientSecret' => '6105d4d8472489d2731e5e7bf006c562',
                ],
            ],
        ]
    ],
    'params' => $params,
];
