<?php

namespace backend\daemons;

use common\models\Chat;
use common\models\User;
use Yii;
use Ratchet\ConnectionInterface;
use consik\yii2websocket\WebSocketServer;
use consik\yii2websocket\events\WSClientEvent;

class ChatServer extends WebSocketServer
{

    public function init()
    {
        parent::init();

        $this->on(self::EVENT_CLIENT_CONNECTED, function(WSClientEvent $e) {
            $e->client->name = null;
        });

        $this->on(self::EVENT_CLIENT_DISCONNECTED, function(WSClientEvent $e) {
            foreach ($this->clients as $chatClient) {
                $chatClient->send( json_encode(['message' => $e->client->name.' has gone']) );
            }
        });
    }


    protected function getCommand(ConnectionInterface $from, $msg)
    {
        $request = json_decode($msg, true);
        return !empty($request['action']) ? $request['action'] : parent::getCommand($from, $msg);
    }

    public function commandChat(ConnectionInterface $client, $msg)
    {
        $request = json_decode($msg, true);
        $result = ['message' => ''];

        if (!$client->name) {
            $result['message'] = 'An error has occurred, try to login again';
        } elseif (!empty($request['message']) && $message = trim($request['message']) ) {

            $user = User::find()->where(['username' => $client->name])->one();

            $chat = New Chat();
            $chat->message = $message;
            $chat->created_by = $user->id;
            $chat->created_at = time();
            $chat->save(false);

            foreach ($this->clients as $chatClient) {
                $chatClient->send( json_encode([
                    'type' => 'chat',
                    'from' => $client->name,
                    'message' => $message,
                    'created_at' => date("Y-m-d H:i:s")
                ]) );
            }

        } else {
            $result['message'] = 'Enter message';
        }

        $client->send( json_encode($result) );
    }

    public function commandSetName(ConnectionInterface $client, $msg)
    {
        $request = json_decode($msg, true);

        if (!empty($request['name']) && $name = trim($request['name'])) {

            $client->name = $name;

            $result = ['message' => $name.' joined'];

        } else {
            $result['message'] = 'An error has occurred';
        }

        foreach ($this->clients as $chatClient) {
            $chatClient->send( json_encode($result) );
        }

    }

}