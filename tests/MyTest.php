<?php

use PHPUnit\Framework\TestCase;

class MyTest extends TestCase
{
    
    protected $calculator;

    public function setUp(): void
    {
        $this->calculator = new Calculator;
    }

    public function testFirstAssertion()
    {
        $this->assertTrue(true);
    }

    public function testCorrectAddition()
    {
        $result = $this->calculator->add(10,2);
        $expected = 12;

        $this->assertEquals($expected, $result);
    }

    public function testCorrectSubstraction()
    {
        $result = $this->calculator->subtract(10,2);
        $expected = 8;

        $this->assertEquals($expected, $result);
    }

    public function testCorrectMultiplication()
    {
        $result = $this->calculator->multiply(10,2);
        $expected = 20;

        $this->assertEquals($expected, $result);
    }

    public function testCorrectDivision()
    {
        $result = $this->calculator->divide(10,2);
        $expected = 5;

        $this->assertEquals($expected, $result);
    }

    public function testGetErrorOnDivisionByZero()
    {
        $this->expectError();

        $result = $this->calculator->divide(10,0);

    }

}