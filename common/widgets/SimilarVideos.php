<?php

namespace common\widgets;

use yii\base\Widget;
use common\models\Video;

class SimilarVideos extends Widget
{
    public $videos = [];
    public $exclude = '';
    public $keyword = '';


    public function init()
    {
        parent::init();
        $this->videos = Video::find()
        ->published()
        ->andWhere(['NOT', ['video_id' => $this->exclude]])
        ->byKeyword($this->keyword)
        ->limit(10)
        ->all();
    }

    public function run()
    {
        return $this->render('@frontend/views/widgets/similar_videos', [
            'videos' => $this->videos
        ]);
    }
}

?>