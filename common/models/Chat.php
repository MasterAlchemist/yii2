<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%chats}}".
 *
 * @property int $id
 * @property string|null $message
 * @property int|null $created_by
 * @property string|null $created_at
 *
 * @property User $createdBy
 */
class Chat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%chats}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // [['created_by'], 'integer'],
            // [['created_at'], 'safe'],
            // [['message'], 'string', 'max' => 255],
            // [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'message' => 'Message',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
}
