<?php
namespace common\components;

use Yii;
use common\models\Auth;
use common\models\User;
use yii\helpers\ArrayHelper;
use yii\authclient\ClientInterface;
use yii\authclient\clients\Facebook;
use yii\authclient\clients\Live;

/**
 * AuthHandler handles successful authentication via Yii auth component
 */
class AuthHandler
{
    /**
     * @var ClientInterface
     */
    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function handle()
    {
        $attributes = $this->client->getUserAttributes();
        $id = ArrayHelper::getValue($attributes, 'id');
        $username = $this->cleanUsername(ArrayHelper::getValue($attributes, 'name'));
        $email = ArrayHelper::getValue($attributes, 'email');

        if($this->client instanceof Live){
            $email = ArrayHelper::getValue($attributes['emails'], 'account');
        }
        
        /* @var Auth $auth */
        $auth = Auth::find()->where([
            'source' => $this->client->getId(),
            'source_id' => $id,
        ])->one();


        if ($auth != null) { // login
            /* @var User $user */
            $user = $auth->user;
            if($this->updateUserInfo($user)){
                Yii::$app->user->login($user, Yii::$app->params['user.rememberMeDuration']);
            } else{
                Yii::$app->getSession()->setFlash('error', [
                    Yii::t('app', "Cannot login a blocked user"),
                ]);
            }
        } else { // signup
            if ($email !== null && User::find()->where(['email' => $email])->exists()) {
                Yii::$app->getSession()->setFlash('error', [
                    Yii::t('app', "User with the same email as in {client} account already exists", ['client' => $this->client->getTitle()]),
                ]);
            } else {
                $password = Yii::$app->security->generateRandomString(6);
                $user = new User([
                    'username' => $username,
                    // 'github' => $nickname,
                    'email' => $email,
                    'password' => $password,
                    'status' => User::STATUS_ACTIVE // make sure you set status properly
                ]);
                $user->generateAuthKey();
                $user->generatePasswordResetToken();

                $transaction = User::getDb()->beginTransaction();

                if ($user->save()) {
                    $auth = new Auth([
                        'user_id' => $user->id,
                        'source' => $this->client->getId(),
                        'source_id' => (string)$id,
                    ]);
                    if ($auth->save()) {
                        $transaction->commit();
                        Yii::$app->user->login($user, Yii::$app->params['user.rememberMeDuration']);
                    } else {
                        $transaction->rollBack();
                        Yii::$app->getSession()->setFlash('error', [
                            Yii::t('app', 'Unable to save {client} account: {errors}', [
                                'client' => $this->client->getTitle(),
                                'errors' => json_encode($auth->getErrors()),
                            ]),
                        ]);
                    }
                } else {
                    $transaction->rollBack();
                    Yii::$app->getSession()->setFlash('error', [
                        Yii::t('app', 'Unable to save user: {errors}', [
                            'client' => $this->client->getTitle(),
                            'errors' => json_encode($user->getErrors()),
                        ]),
                    ]);
                }
            }
        }
        
    }

    /**
     * @param User $user
     */
    private function updateUserInfo(User $user)
    {
        if($user->status === User::STATUS_INACTIVE){
            $password = Yii::$app->security->generateRandomString(6);
            $user->password = $password;
            $user->status = User::STATUS_ACTIVE;
            return $user->save();
        }

        return $user->status !== User::STATUS_DELETED;

    }

    private function cleanUsername($username)
    {
        $username = str_replace(' ','_',$username);
        $username = preg_replace('/[^A-Za-z0-9_]/', '', $username);
        return $this->duplicatedUsername($username);
    }

    private function duplicatedUsername($username)
    {
        if($username !== null && User::find()->where(['username' => $username])->exists()) {
            $random = random_int(0,9);
            $username .= $random;
            return $this->duplicatedUsername($username);
        } else{
            return $username;
        }
    }
}