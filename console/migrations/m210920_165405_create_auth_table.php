<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%auth}}`.
 */
class m210920_165405_create_auth_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%auth}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11)->notNull(),
            'source' => $this->string(512)->notNull(),
            'source_id' => $this->string(512)->notNull(),
        ]);

        $this->addForeignKey(
            'fk-auth-user_id-user-id',
            'auth', 
            'user_id', 
            'user', 
            'id', 
            'CASCADE', 
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-auth-user_id-user-id', '{{%auth}}');
        $this->dropTable('{{%auth}}');
    }
}
