<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%chats}}`.
 */
class m211004_222727_create_chats_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%chats}}', [
            'id' => $this->primaryKey(),
            'message' => $this->string(255),
            'created_by' => $this->integer(11),
            'created_at' => $this->integer(11),
        ]);

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-chats-created_by}}',
            '{{%chats}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%chats}}');

        $this->dropForeignKey(
            '{{%fk-chats-created_by}}',
            '{{%chats}}'
        );
    }
}
