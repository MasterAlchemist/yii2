<?php

use yii\db\Migration;

/**
 * Class m210917_152428_create_fulltext_index_on_video
 */
class m210917_152428_create_fulltext_index_on_video extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        return $this->execute("ALTER TABLE {{%video}} ADD FULLTEXT INDEX search(title, description, tags)");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return $this->execute("ALTER TABLE {{%video}} DROP INDEX search");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210917_152428_create_fulltext_index_on_video cannot be reverted.\n";

        return false;
    }
    */
}
