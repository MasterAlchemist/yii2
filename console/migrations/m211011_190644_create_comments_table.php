<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%comments}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%video}}`
 * - `{{%comments}}`
 * - `{{%user}}`
 */
class m211011_190644_create_comments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%comments}}', [
            'id' => $this->primaryKey(),
            'comment' => $this->text()->notNull(),
            'pinned' => $this->smallInteger()->defaultValue(0),
            'video_id' => $this->string(16)->notNull(),
            'parent_id' => $this->integer(11),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'created_by' => $this->integer(11),
        ]);

        // creates index for column `video_id`
        $this->createIndex(
            '{{%idx-comments-video_id}}',
            '{{%comments}}',
            'video_id'
        );

        // add foreign key for table `{{%video}}`
        $this->addForeignKey(
            '{{%fk-comments-video_id}}',
            '{{%comments}}',
            'video_id',
            '{{%video}}',
            'video_id',
            'CASCADE'
        );

        // creates index for column `parent_id`
        $this->createIndex(
            '{{%idx-comments-parent_id}}',
            '{{%comments}}',
            'parent_id'
        );

        // add foreign key for table `{{%comments}}`
        $this->addForeignKey(
            '{{%fk-comments-parent_id}}',
            '{{%comments}}',
            'parent_id',
            '{{%comments}}',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-comments-created_by}}',
            '{{%comments}}',
            'created_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-comments-created_by}}',
            '{{%comments}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%video}}`
        $this->dropForeignKey(
            '{{%fk-comments-video_id}}',
            '{{%comments}}'
        );

        // drops index for column `video_id`
        $this->dropIndex(
            '{{%idx-comments-video_id}}',
            '{{%comments}}'
        );

        // drops foreign key for table `{{%comments}}`
        $this->dropForeignKey(
            '{{%fk-comments-parent_id}}',
            '{{%comments}}'
        );

        // drops index for column `parent_id`
        $this->dropIndex(
            '{{%idx-comments-parent_id}}',
            '{{%comments}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-comments-created_by}}',
            '{{%comments}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-comments-created_by}}',
            '{{%comments}}'
        );

        $this->dropTable('{{%comments}}');
    }
}
