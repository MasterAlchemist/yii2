<?php

use yii\db\Migration;

/**
 * Class m210930_165149_add_fk_videos_category_id_to_videos
 */
class m210930_165149_add_fk_videos_category_id_to_videos extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            '{{%fk-videos-category_id}}',
            '{{%video}}',
            'category_id',
            '{{%categories}}',
            'id',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            '{{%fk-videos-category_id}}',
            '{{%video}}'
        );
    }

}
