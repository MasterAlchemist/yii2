<?php

use yii\db\Migration;

/**
 * Class m211008_213330_create_permissions
 */
class m211008_213330_create_permissions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        // add "manageVideo" permission
        $manageVideo = $auth->createPermission('manageVideo');
        $manageVideo->description = 'Manage a Video';
        $auth->add($manageVideo);

        // add "viewVideo" permission
        $viewVideo = $auth->createPermission('viewVideo');
        $viewVideo->description = 'View a Video';
        $auth->add($viewVideo);

        // add "createVideo" permission
        $createVideo = $auth->createPermission('createVideo');
        $createVideo->description = 'Create a Video';
        $auth->add($createVideo);

        // add "updateVideo" permission
        $updateVideo = $auth->createPermission('updateVideo');
        $updateVideo->description = 'Update Video';
        $auth->add($updateVideo);
        
        // add "deleteVideo" permission
        $deleteVideo = $auth->createPermission('deleteVideo');
        $deleteVideo->description = 'Delete Video';
        $auth->add($deleteVideo);

        // add "author" role and give this role the "createVideo" permission
        $user = $auth->createRole('user');
        $auth->add($user);
        $auth->addChild($user, $manageVideo);
        $auth->addChild($user, $viewVideo);
        $auth->addChild($user, $createVideo);
        $auth->addChild($user, $updateVideo);
        $auth->addChild($user, $deleteVideo);

        // add "admin" role and give this role the "updateVideo" permission
        // as well as the permissions of the "author" role
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $user);

        // Assign roles to users. 1 and 2 are IDs returned by IdentityInterface::getId()
        // usually implemented in your User model.
        // $auth->assign($user, 2);
        // $auth->assign($admin, 1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        $auth->removeAll();
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211008_213330_create_permissions cannot be reverted.\n";

        return false;
    }
    */
}
