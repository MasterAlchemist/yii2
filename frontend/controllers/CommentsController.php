<?php

namespace frontend\controllers;

use yii\web\Controller;
use common\models\Comment;
use yii\filters\AccessControl;

class CommentsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['create'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ],
        ];
    }

    public function actionCreate()
    {
        $model = new Comment;

        if ($model->load($this->request->post(), '') && $model->save()) {
            return $this->renderPartial('@frontend/views/video/_comment_item', [
                'model' => $model
            ]);
        }
        
    }
}