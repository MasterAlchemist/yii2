<?php

/**
 * @var $videos common\models\Video[]
 */
use yii\helpers\Url;

?>
<?php foreach($videos as $video): ?>

    <div class="media">
        <a href="<?php echo Url::to(['/video/view', 'id' => $video->video_id]) ?>">
            <div class="embed-responsive embed-responsive-16by9 mr-2 thumbnail">
                <img src="<?php echo $video->getThumbnailLink() ?>" class="embed-responsive-item" alt="">
            </div>
        </a>
        <div class="media-body">
            <h6 class="mt-0 mb-0"><?php echo $video->title ?></h6>
            <p class="mb-0">
                <a href="<?php echo Url::to(['/channel/view', 'username' => $video->createdBy->username ]) ?>" class="channel-link">
                    <?php echo $video->createdBy->username ?>
                </a>
            </p>
            <p>
                <?php echo $video->getViews()->count() ?> views · 
                <?php echo Yii::$app->formatter->asRelativeTime($video->created_at) ?>
            </p>
        </div>
    </div>

<?php endforeach; ?>
