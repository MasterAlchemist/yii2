<?php

?>

<div class="media mb-3">
    <img src="https://dummyimage.com/90/ffffff/000000" class="mr-3 comment-avatar" alt="...">
    <div class="media-body">
        <h6 class="mt-0">
            <a class="text-dark" href="#"><?php echo $model->createdBy->username ?></a>
            <small class="text-muted"><?php echo Yii::$app->formatter->asRelativeTime($model->created_at) ?></small>
        </h6>
        <p><?php echo $model->comment ?></p>
    </div>
</div>