<?php
/**
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

use yii\helpers\Url;
use yii\bootstrap4\Html;
use yii\widgets\ListView;
use yii\bootstrap4\Dropdown;
use yii\helpers\ArrayHelper;

$sort = ArrayHelper::getValue($_GET,'sort');
$keyword = ArrayHelper::getValue($_GET,'keyword');

?>

<h1>Results</h1>

<?php echo Html::beginForm(
    Url::to(['video/search']),
    'get'
) ?>
<?php echo Html::hiddenInput('keyword',$keyword) ?>
<?php echo Html::dropDownList(
    'sort', 
    $sort, 
    [
        '' => '',
        'title' => 'Title ASC',
        '-title' => 'Title DESC',
    ], 
    ['onchange'=>'this.form.submit()']
) ?>
<?php echo Html::endForm() ?>


<?php echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_video_item',
    'layout' => '<div class="d-flex flex-wrap">{items}</div>{pager}',
    'itemOptions' => [
        'tag' => false
    ]
]) ?>