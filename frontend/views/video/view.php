<?php

/**
 * @var $model common\models\Video
 * @var $comments common\models\Comment
 */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use common\widgets\SimilarVideos;

?>

 <div class="row">
     <div class="col-sm-8">
        <div class="embed-responsive embed-responsive-16by9">
            <video class="embed-responsive-item" 
                src="<?php echo $model->getVideoLink() ?>" 
                allowfullscreen
                poster="<?php echo $model->getThumbnailLink() ?>"
                controls>
            </video>
        </div>
        <h6 class="mt-2"><?php echo Html::encode($model->title) ?></h6>
        <div class="d-flex justify-content-between align-items-center">
            <div>
                <?php echo $model->getViews()->count() ?> views · <?php echo Yii::$app->formatter->asDate($model->created_at) ?>
            </div>
            <div>
                <?php Pjax::begin() ?>
                    <?php echo $this->render('_buttons',[
                        'model' => $model
                    ]) ?>
                <?php Pjax::end() ?>
            </div>
        </div>
        <div>
            <p>
                <a href="<?php echo Url::to(['/channel/view', 'username' => $model->createdBy->username ]) ?>" class="channel-link">
                    <?php echo $model->createdBy->username ?>
                </a>
            </p>
            <p>
                <?php echo Html::encode($model->description) ?>
            </p>
        </div>
        <div class="comments mt-4">
            <div class="create-comment mb-3">
                <div class="media">
                    <img src="https://dummyimage.com/90/ffffff/000000" class="mr-3 comment-avatar" alt="...">
                    <div class="media-body">
                        <form id="create-comment-form" action="<?php echo Url::to(['/comments/create']) ?>" method="post">
                            <input type="hidden" name="video_id" value="<?php echo $model->video_id ?>">
                            <textarea id="leave-comment" name="comment" id="" rows="1" class="form-control" placeholder="Add a public comment"></textarea>
                            <div class="comment-buttons text-right mt-2">
                                <button type="button" id="cancel-comment" class="btn btn-light">Cancel</button>
                                <button id="submit-comment" class="btn btn-light">Confirm</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div id="comment-wrapper" class="comment-wrapper">
                <?php foreach($comments as $comment){
                    echo $this->render('_comment_item', [
                        'model' => $comment
                    ]);
                } ?>
            </div>
        </div>
     </div>
     <div class="col-sm-4">
        <?= SimilarVideos::widget(['keyword' => $model->title, 'exclude' => $model->video_id]) ?>
     </div>
 </div>