<?php

/**
 * @var $model \common\models\Video
 */

use yii\helpers\Url;

?>
<div class="card m-3 video-item">
    <a href="<?php echo Url::to(['/video/view', 'id' => $model->video_id]) ?>">
        <div class="embed-responsive embed-responsive-16by9">
          <img src="<?php echo $model->getThumbnailLink() ?>" class="embed-responsive-item" alt=""> 
        </div>
    </a>
    
  <div class="card-body p-2">
    <h6 class="card-title m-0"><?php echo ucfirst($model->title) ?></h6>
    <p class="text-muted card-text m-0">
      <a href="<?php echo Url::to(['/channel/view', 'username' => $model->createdBy->username ]) ?>"  class="channel-link">
          <?php echo $model->createdBy->username ?>
      </a>
    </p>
    <p class="text-muted card-text m-0"><?php echo $model->getViews()->count() ?> views · <?php echo Yii::$app->formatter->asRelativeTime($model->created_at) ?></p>
  </div>
</div>