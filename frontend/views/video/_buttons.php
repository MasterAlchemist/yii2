<?php

/**
 * @var $models \common\models\Video
 */

use yii\helpers\Url;

?>
<a data-method="post" data-pjax="1" class="btn btn-sm <?php echo $model->isLikedBy(Yii::$app->user->id) ? 'btn-outline-primary' : 'btn-outline-secondary' ?>" href="<?php echo Url::to(['/video/like', 'id' => $model->video_id]) ?>">
    <i class="fas fa-thumbs-up"></i> <?php echo $model->getLikes()->count() ?>
</a>
<a data-method="post" data-pjax="1" class="btn btn-sm <?php echo $model->isDislikedBy(Yii::$app->user->id) ? 'btn-outline-primary' : 'btn-outline-secondary' ?>" href="<?php echo Url::to(['/video/dislike', 'id' => $model->video_id]) ?>">
    <i class="fas fa-thumbs-down"></i> <?php echo $model->getDislikes()->count() ?>
</a>