<?php

use yii\helpers\Url;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

NavBar::begin([
    'brandLabel' => '<image src="'.Yii::$app->params['frontendURL'].'images/unotube.png" style="height:1.5em" class="mr-1">'.Yii::$app->name,
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-expand-sm navbar-light bg-light shadow-sm',
    ],
]);
if (Yii::$app->user->isGuest) {
    $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
    $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
} else {
        $menuItems[] = [
            'label' => 'Logout ('.Yii::$app->user->identity->username.')',
            'url' => ['/site/logout'],
            'linkOptions' => [
                'data-method' => 'post'
            ]
        ];
}
?>
<form class="form-inline my-2 my-lg-0" action="<?php echo Url::to(['/video/search']) ?>">
    <div class="input-group">
        <input name="keyword" type="text" class="form-control" placeholder="Search" value="<?php echo Yii::$app->request->get('keyword') ?>">
        <div class="input-group-append">
            <button class="btn btn-outline-secondary search-button" id="button-addon2"><i class="fas fa-search"></i></button>
        </div>
    </div>
</form>
<?php
echo Nav::widget([
    'options' => ['class' => 'navbar-nav ml-auto'],
    'items' => $menuItems,
]);
NavBar::end();