<?php
/**
 * @var $channel \common\models\User
 * @var $dataProvider \yii\data\ActiveDataProvider;
 */

use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\widgets\ListView;

?>
<div class="jumbotron">
    <div class="media">
        <img src="https://dummyimage.com/90/ffffff/000000" class="mr-3 rounded-circle" alt="<?php echo $channel->username ?>">
        <div class="media-body">
            <h1 class="display-4"><?php echo $channel->username ?></h1>
        </div>
    </div>
    <hr class="my-4">
    <p>
        <?php echo $channel->getSubscribers()->count() ?> subscribers
    </p>
    <?php Pjax::begin() ?>
        <?php echo $this->render('_subscribe', [
            'channel' => $channel
        ]) ?>
    <?php Pjax::end() ?>
</div>

<?php echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '@frontend/views/video/_video_item',
    'layout' => '<div class="d-flex flex-wrap">{items}</div>{pager}',
    'itemOptions' => [
        'tag' => false
    ]
]) ?>