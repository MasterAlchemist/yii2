$(function(){

    const $cancelComment = $('#cancel-comment');
    const $leaveComment = $('#leave-comment');
    const $createCommentForm = $('#create-comment-form');
    const $commentWrapper = $('#comment-wrapper');

    $leaveComment.click(function(){
        $leaveComment.
            attr('rows','2')
            .closest('.create-comment')
            .addClass('focused');
    });

    $cancelComment.click(resetForm);

    $createCommentForm.submit(function(ev){
        ev.preventDefault();
        
        $.ajax({
            method: 'post',
            url: $createCommentForm.attr('action'),
            data: $createCommentForm.serializeArray(),
            success: function(response){
                $commentWrapper.prepend(response);
                resetForm();
            }
        });
    });

    function resetForm(){
        $leaveComment
            .val('')
            .attr('rows','1');
        $cancelComment
            .closest('.create-comment')
            .removeClass('focused');
    }


});